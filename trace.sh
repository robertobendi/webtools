#!/bin/bash

if [[ "" == "$1" ]]; then
  echo "inserire un nome host o indirizzo ip"
  exit
fi

traceroute $1 | awk '{print $3}' | tr -d '(' | tr -d ')'