#!/bin/bash

if [[ "" == "$1" ]]; then
  echo "inserire un nome host o indirizzo ip per verificarne l'attivit�"
  exit
fi

ping $1  -w 1 | tail -n 2 | head -n 1 | awk '{print $4}'