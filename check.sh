#!/bin/bash

if [[ "" == "$1" ]]; then
	echo "inserire indirizzo ip o nome host"
	exit
fi

if [[ "" == "$2" ]]; then
	echo "inserire il numero di porta tcp"
	exit
fi

risp=$(nc -z $1 $2 ; echo $?)
if [[ "0" == "$risp" ]]; then
	echo " ACTIVE"
else
	echo " CLOSED"
fi

